===============================================================================
                        *** TRACK INFORMATION ***
===============================================================================
Author		: Gel38
Email Address	: aaron.gel38@gmail.com or gel_lr34@hotmail.com
Track name	: "Rally ZX SS 2"
Filename	: 'Re-Volt\levels\RallyZX2'
Track Type	: Extreme
Track length	: 630 meters.
Textures (if any)	: Features ALL NEW Gravel/Concrete, snow, ice, snow/grave transition, ice/gravel transition, sand, checkpoints, and metal textures. Other textures borrowed from the internet. Tree textures borrowed from Lake Slide by Ricky D.
Extra Info	: Totally new track. All 3d models herein are COMPLETELY new. (even the trees, as i wanted to learn how to make them fairly properly) Model list is as follows:

		Banner.prm (FALKEN)
		BannerK.prm (Klein Tools)
		green.prm (checkpoint)
		logs.prm
		people.prm
		pipes.prm
		powerline.prm
		red.prm (checkpoint)
		rocks.prm
		Trailor.prm
		Treeplane.prm (used for added dimension for side walls)
		yellow.prm (checkpoint)
		
Completion Date	: March 30th, 2014

Additional Credits to	: Yahweh, Yeshua, and the Holy Ghost (The first 2 are God and Jesus) - First and foremost!)
			Big thanks to Jigebren, who had mercy on me when 3D Studio Max didn't work for me on Vista...
			so he sent me the blender export
			THANKS JIGEBREN! :D
			And YOU for downloading this track!!!
			The Creators of Colin McRae Rally 2.0, as most of my inspiration was drawn from your game!

Other words	: Rally ZX SS 2

**Update**
-Added reposition triggers. I had thought about adding them in, but thought, "Na, noone's gonna cut on the upper side of the track"
Well, I added it in because... well, LastCuban is to thank for it (his Youtube videos). Sorry cuban! :D At any rate! It makes it more
like a rally game. They repo you when you run into the crowd.

It's been a few years since the first Rally ZX. (which, btw, did you know that the 1st RZX was the FIRST extreme track made in Zmodeler?)
Anyway! Since the release of the 1st RZX i've attempted to make a 2nd release of it, but found none worthy enough to be it. I always felt
the track was kind of an underdog thing for me. I loved it personally. Still do. It's just, the modeling is on the low end. Anyway.
I really wanted to up the ante with my modeling for RZX2 So! Here it is. I hope you guys totally enjoy this track. 
===============================================================================


* Description *
---------------
Based off of the Colin McRae Rally game series'. 

* Tips/Hints *
--------------
THERE IS A HIDDEN PRACTICE STAR on the level. There is also a battle star, hidden.


* Installation *
----------------
Unzip into your Re-Volt directory. all files will go where they should.

* Copyright/Permissions *
-------------------------
All models are original 
IF YOU USE ANY OF THE ITEMS (Textures, models, custom made by me)... Please mention me in the credits.
If you wish to use a portion of this track in your track, please ask me first. My email is above.
If you wish to modify a mesh... DONT! Unless you have my permission. (if improvements are good i may let you do them!)

You may distribute this track ONLY if u include this file with NO modifications. Do not sell or exploit this track it was made for everyone to enjoy, not those who can afford it.