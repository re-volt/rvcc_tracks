---------------------------WINTER PARK -> README.txt---------------------------

Track  :	Winter Park
Author :	Zach K. (zagames)
Folder :	winterpark
Length :	438 meters
Skill  :	Easy
Type   :	Extreme

---------------------------WINTER PARK -> README.txt---------------------------

Notes  :	If you wish to include parts of this track in your own, feel
			free to do so, but please give me credit in your README
			file.

---------------------------WINTER PARK -> README.txt---------------------------

Tools  :	3DS Max 8
		(Microsoft) Paint
		RVTMOD7 (ase2w)
		Re-Volt (MAKEITGOOD)
		Adobe Photoshop CS

---------------------------WINTER PARK -> README.txt---------------------------

Credits:	Davegh for some basic texture ideas.
		AGT for the GFX image.

---------------------------WINTER PARK -> README.txt---------------------------

Website:	http://revolt.zackattackgames.com		(Mine)
		http://revolt.speedweek.net/main/news.php	(RVZT)
		http://z8.invisionfree.com/RRR_Racing_Forum	(Phoenix R3)

---------------------------WINTER PARK -> README.txt---------------------------

Misc.  :	Everything in this track is 100% original.  :D
		Please let me know what you like best about this track on RVZT.
		If you are into Phoenix R3, a FC version should be available
			sometime soon.
