Credits to:

Music: Dancing snowflakes by Madboss

Contains assets from ambientCG.com, licensed under CC0 1.0 Universal. Some modified by me.

Cars featured:
Triton by Burner 94, Norfair, Skarma
Tripeak and Piercer by Paperman
Kanberra Krusier by Xarc