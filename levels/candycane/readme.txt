 
 Candy Cane Lane --- Christmas 2007
 ----------------------------------
 by hilaire9
 ------------------ 
 Length: 683 meters                                      
 Type: Extreme
 ======================================================
 *** To Install: Unzip into your main Re-Volt folder. 
 -------------------------------------------------------------
 Tools/Credit: 3ds Max 5, ASE Tools, rvGlue, MAKEITGOOD edit modes,
 ZModeler v1.07a, and Paint Shop Pro 9.
  
 Custom model prms by zagames, RST, Yamada, and hilaire9. 
 
 Santa and sleigh model prms modified from Santa�s Hot Rod Sleigh by Iron Bob. 
 ------------------------------------------------------------------------------
 --- hilaire9's Home Page: http://members.cox.net/alanzia 
 
 --- zagames' website: http://revolt.zackattackgames.com
 
 --- RST's website: http://z8.invisionfree.com/RRR_Racing_Forum
 ------------------------------------------------------------------------------
