
Track info
----------
Track name:             The glaciers
Track length:           313 m
Type:              	Extreme
Author:                 Allan1
Time:			Day
Weather:		Cold
contact:                allan.mitestainer@yahoo.com.br
Date:			21th jan 11
Reversed Mode:		available


Description
-----------
A race to cool off! It's set in a village on Alaska, around a glacier.
Everything are covered with snow and the terrain are bumpy.


Installation
-----------------------------

Unzip Glaciers.zip into your Re-Volt directory.


Reversed Mode Installation
-----------------------------

'The Glaciers' can be played in reversed mode.

Acess the batch file ms-dos 'install_glaciers_reversed.bat'.

Which creates a new track named 'the glaciers reversed', by
copying reversed mode files into a new folder. This way
you can race reversed without overwriting an original track.


Instances Credits						
-----------------------------				

Huts		by BurnRubr

Rocks								
Ground							
Mound		by RickyD			
Fence
Burned Tree							 					

Tree			
Ramp		from re-volt			
Kale							
							
snow dool						
kichen garden	by Zach
Pickup4x4
																		   
Novlite		by Crescviper
							
		   scloink	
		   Yamada	
Buffalo		by SuPeRTaRD		
		   The Me and Me


Used Tools
-----------------------------

Paint
RV Glue
MAKEITGOOD Edit Mode
CorelDRAW 10
CorelPHOTO-PAINT 10
Re-volt Track Editor



Thanks for playing my track
Allan1