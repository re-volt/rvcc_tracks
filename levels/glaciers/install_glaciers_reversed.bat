
@echo off
cls
echo You are about to install track 'The Glaciers Reversed'. This will be a new
echo track (a copy of 'The Glaciers'), in a separate directory 'levels\glaciers_r'.
echo This is the only way to play 'The Glaciers' reversed without overwriting an
echo original Acclaim track.
echo.
echo No existing data will be deleted or modified.
echo.
echo You MUST quit Re-Volt before continuing (or some files cannot be copied).
echo.
echo Do you want to  continue
choice
if errorlevel 2 goto done_no_changes

if exist ..\glaciers_r\nul goto have_old

rem *** creating new track folder and copying files ***
cd ..\..
mkdir levels\glaciers_r
copy levels\glaciers\*.* levels\glaciers_r
rem *** replacing forward files with reversed ones ***
copy levels\glaciers\reversed\*.* levels\glaciers_r
rem *** installing track selection bitmaps ***
move levels\glaciers_r\glaciers_r.bmp gfx\glaciers_r.bmp
rem *** renaming files ***
cd levels\glaciers_r
ren glaciers.cam glaciers_r.cam
ren glaciers.fan glaciers_r.fan
ren glaciers.fin glaciers_r.fin
ren glaciers.fob glaciers_r.fob
ren glaciers.inf glaciers_r.inf
ren glaciers.lit glaciers_r.lit
ren glaciers.ncp glaciers_r.ncp
ren glaciers.pan glaciers_r.pan
ren glaciers.taz glaciers_r.taz
ren glaciers.tri glaciers_r.tri
ren glaciers.vis glaciers_r.vis
ren glaciers.w glaciers_r.w
ren glaciersa.bmp glaciers_ra.bmp
ren glaciersb.bmp glaciers_rb.bmp
ren glaciersc.bmp glaciers_rc.bmp
ren glaciersd.bmp glaciers_rd.bmp
ren glacierse.bmp glaciers_re.bmp
ren glaciersf.bmp glaciers_rf.bmp
ren glaciersg.bmp glaciers_rg.bmp
ren glaciersh.bmp glaciers_rh.bmp
ren glaciersi.bmp glaciers_ri.bmp
ren glaciersj.bmp glaciers_rj.bmp
rem *** this file is unneeded in reversed the glaciers ***
del Install_glaciers_Reversed.bat
cd ..\glaciers
goto done


:have_old
echo.
echo 'levels\glaciers_r' already exists, cannot continue. If you want to re-install
echo 'the glaciers reversed', you have uninstall first:
echo  - delete levels\glaciers_r
echo  - delete gfx\glaciers_r.bmp
echo  - delete gfx\glaciers_r.bmq
goto done_no_changes


:done_no_changes
echo Nothing is done, no files were copied.
goto end


:done
echo.
echo Successfully created track 'the glaciers reversed' in folder 'levels\glaciers_r'.
goto end


:end