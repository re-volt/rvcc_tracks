================
Main information
================

Track name: Morso Cave
Track authors: Ahma, rodik
Track length: 456m
Time Trial time: 0:45:000
Reverse Version: yes
Practice star: yes
Track difficulty: Extreme

==========
Developers
==========

Ahma:
-Track Director
-Track Construction
-AI Nodes
-Pos Nodes
-Track Zones
-Triggers
-Pickup Locations

Rodik: 
-Blender Editing (Track Model)
-Textures
-Objects
-Instances
-Track Optimization (Visiboxes, NCP Optimization)

Zeino:
-Morso
-Racing line optimization

=====
Stats
=====

World Faces: 25,844
With Instances: 40,938
NCP Faces: 19,676



============
Track Tester
============
Virus


===============
About the track
===============
Curious campers are trying to get a glimpse of a legendary creature, Morso.


==================
Credits and thanks
==================

Many thanks to Keyran for the texture animation of the river
Thanks to Xarc for the other texture animations.
Eagle custom animation by G_J (Original Bird animations from Rooftops 1)
Flag custom animation by Kiwi (Original Norway Flag animations from Spitzbergen)
Tent 3D model: https://www.turbosquid.com/ru/3d-models/camping-tent-model-2023671 (Edited by rodik)
Music: String, Skins and Metal by Matthew Reid (https://www.youtube.com/watch?v=z5-uVNlLAVA)

==========
Tools used
==========

Modeling: 
Blender 2.79b (with Marv's Blender Plugin)
WorldCut 11-11-11

Texture Editing:
Krita 
Adobe Photoshop 2023

Music Editing:
FL Studio 12

Misc: 
Notepad++
In-game MIG
OBS Studio



=================
Other information
=================

-You cannot use this track for commercial purposes
-You can modify and redistribute the track as long as you credit the creators.

=================
Contact (Discord, email)
=================

Ahma: vakava_ahma, vakava.ahma@gmail.com
Rodik: rodikq