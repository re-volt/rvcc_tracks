
----Track info-----
Track name	: Glacier Cliffs 3 
Track type	: Extreme
Length		: 363m
Author		: Yamada

Description:
 First of all, I have to say great thanks to Shown64. The Basic idea and most textures are done by him. Thank you very much, Shown!
 Well, now about this track, it is a iceberg on the ocean. It's short and simple, but may have some dificulty, mostly because of the icerocks. And also you have to keep out of the sea. Or you'll fall into the dark and cold seabed! ;) Good luck!

(and all ices, icerocks, iceramp and icicles are free to use!)

Installation:
 Unzip the file into your Re-Volt directory. (Defaultly "\Program Files\Acclaim entertainment\Re-Volt")

Thanks to:
	Shown64 for idea of icy tracks, the glacier textures, and allowing me to use the name "Glacier Cliffs"
	JimK for the off-road kit (including textures)
	Ali, Mr.Sandin, and Gabor for RVTMOD4
	Chaos for rv-sizer
	Gibbler for RV-Extreme site
	Antimorph for beta test
	Mete Ciragan for MilkShape
	Oleg.M for Zmodeler
	HouseMarque for ice texture out of the game "Supreme Snowboarding"
	Probe for Re-Volt
	And all Re-Volt fans!
	

	and all Re-Volt fans.