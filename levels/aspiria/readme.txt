===============================================================================
                        *** TRACK INFORMATION ***
===============================================================================

Track name              : Aspiria
Filename                : Aspiria.zip
Track length            : 536 meters
Size                    : Single
New textures            : Yes, all of them
Version                 : 1.00
                                      
Author                  : Aeonics Inc.
Email Address           : archakelon@gmail.com

===============================================================================


* Description *
---------------
This track is pure inspiration-based. The idea hit me one day for a track layout that sounded kind of neat, so I put it together in the editor (which is unusual for me because usually I want to draw them out and make sure everything fits before doing that) and it turned out pretty nice. So I decided I'd keep it and work on it later, since I already had another track project that I was working on.

Well, later came sooner than later because I started wanting to do things with this track. So the first thing I did was come up with a theme - I knew I wanted either a snow, forest, or ocean. Well I started fiddling with textures, and snow was the first texture that came out nicely. After that, I thought, you know what would be cool? Since this is a pretty straightforward track, what if the entire track was riddled with boosts? That'd be interesting! So I did that, and it worked reasonably well. And, well, one thing led to another and after adding lots of lights, AI, nodes, fields, instances, objects, and so forth, here it is!

Now I'll be the first to say that the AI isn't too bright about knowing when to and when not to hit the boosts. So you - the human player - will have an advantage after you've learned the track, because you can decide when you've got enough control to manage more speed. The AI just hits a boost and hopes it can stay on course. Hopefully I've done a good enough job on it that it'll be pretty competant for the first few races at least. :)


* Tips/Hints *
--------------
You don't need to hit all of the boosts. Actually I don't recommend it. Unless you've got an exceptionally maneuverable car, at which point there are some sweet combinations you can take - up to 4 boosts in a row - to get some massive leads over your opponents. Typically, though, you want to avoid any boosts that are on the inside of a turn. They probably won't help you. Don't take them unless you've just crashed and you need some help accelerating back to speed.


* Bugs/Problems *
-----------------
There's some graphical issues with the bridge object. You're not really supposed to know where the bridge object is, so don't worry too much about it. I'm just letting you know that its there. It was something that I didn't really have any way to fix without creating other problems.


* Installation *
----------------
Just unzip all files into your Re-Volt folder, they will automatically be extracted into the right subfolders. Start the game, choose the track and go!


* Copyright/Permissions *
-------------------------
You are NOT allowed to commercially exploit this track, i.e. put it on a CD or any other electronic medium that is sold for money. I'm also not sure where the textures came from, so you probably shouldn't use them. :)


* Thanks and credits go to *
----------------------------
The Me and Me, because I used their car models was a basis for the cars I tested this track with. Slightly Askew, since his/her track inspired me. Re-Volt Archive and ManMountain, for providing information on using MAKEITGOOD. srmalloy for providing Wscale. Alexander Kr�ller, Christer Sundin and G�bor Varga for rvglue. And the Re-Volt Tracks site for hosting this track, hopefully. :) Also thanks to Acclaim for creating, not only this game, but Extreme-G as well. This track was partially inspired by Extreme-G 3's Crystalaria track. (In fact, I listened to the Crystalaria music on mp3 about half the time I was working on this.)
