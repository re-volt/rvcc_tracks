------------------------------------------------------------------
Ye Olde Fun Shoppe by Scloink and SuperTard
------------------------------------------------------------------
Folder Name: Shoppe

Author/Nickname: Scloink & Supertard

Email: Kukri@aol.com        

Track Length: 774 meters


Story:
Scloink and SuperTard being good friends decided one day to open a business together. They gathered all their resources(as if they have any) and went into the planning stages of their business. Scloink liking to race wanted to open a Hobby Shop while SuperTard, who is from the old school wanted some type of arcade/Diner combination. They argued back and forth on what to do then one day they came up with a brilliant idea. "Why not make it both" they said in union. So they sat down and designed their building. SuperTard would get his arcade and Diner while Scloink would get his Hobby Shop. They gave it the name Ye Ole Fun Shoppe. SuperTard one day while they were getting their first shipment in said to Scloink "We need a way to attrack some attention to us". "Why don't we host a RC Grand Prix which will take place in the Shoppe and work its way throughout the various rooms in here" Scloink said. "Thats a great idea" SuperTard said.
Present Day:
It is time. The race is about to begin. All you need to do is install the necessary files and you are ready to rumble. The winner gets a nice shiny gold plated Trout. The looser gets smacked with said Trout. Hehe 
3
2
1
GO!!!!!
 

Track Installation: 
Just unzip to your revolt directory and all files will be placed accordingly.
To install the skymap click on the install shoppe skymap bat file. It will replace 
the nhood1 skymap and make a backup. I chose nhood1 because this skymap looks good
in there too. I would recommend this skymap or any one that has a city to it. If you 
don't use a city based skymap then it will look empty when you look out the windows.


Description: 
A pretty complex track. It will take a few laps before you get the hang of the flow. There are a few areas where you will notice a serious drop in fps. I along with Nick and Skitch have looked into this and there are alot of visiboxes in place. Maybe I don't understand them fully but to the best of my understanding they are placed well. If anyone would like to look into the fps problem or if anyone has a solution then please let me know.
I know the track is not to an exact scale but believe me if you had seen the original which was to scale you would thanks me. I know there are a couple of textures that don't tile well but I have adjusted as much as I can in max and no matter what I do they still will not tile even though they tile well otherwise.


Tips: 
If you know you are gonna fall off the shelf hit reset immediately. There are no repo triggers in place. 


Thanks:
Thanks to the whole community for their support of this great game and their patience with me for taking so long with this.

 
Special Thanks go out to the follwing people:
Antimorph - Without his help in Max I would have never finished it.
Triple6s - For testing and being there when I needed a lift.
Nick - For help with the visiboxes.
Skitch - For help with Max and visiboxes.
Cybinary - For testing.
LaserBeams - For advice in Max and help with testing and lighting.
Spaceman - For testing and a few textures as well as some advice on design.


Visit: The Racers Point Forum and RHQ if you havn't already. 
------------------------------------------------------------------

------------------------------------------------------------------
