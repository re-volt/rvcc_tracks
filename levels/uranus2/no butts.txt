Track: Uranus
Authors: Biometal/Spriggan

Length: 907M
Difficulty: Extreme

Environment: Space



Uranus holds the 7th spot in the Solar System. Constantly being ridiculed and made fun of because of its name, this icy planet has much in store for its outside visitors. The Uranian world is shrouded in a thick fog that covers a vast range of visibility as its luscious, gaseous clouds waft throughout the environment. It's a lonely world---mostly known for being "The Butt of Space"---and yet still has a few non-flatulent beauties about it. It's apparently only been visited once by a space probe (the Voyager 2), but only as a mere flyby. If you're one of those imbecilic, coarse-minded blow-hards who can't resist belittling this gas giant, then you're in for "quite a ride" once you've come here.



Present in the track are: slick slopes, chasms, frigid tunnels, and several other solidified Uranian materials.

[***Uranian Racing Survival Guide for Dummies***]: Traverse through the sleek tube of frozen Uranian fluid, down the half-pipe slide, and up into the Shulu nests. Afterward, you will need to take caution while leaping from giant ice platforms and down into the intestinal track of the Golgik (yes, it is a sentient alien that eats and defecates you), followed by carefully completing the lap after a few more leaps of faith.



[Uranian species whom will be spectating are]:

Fluttering Fungi: Crustaceous-looking beings that are not sentient. They spend their Uranian lifetimes getting carried along by the winds that blow throughout their home. While Uranian winds are clocking around 560mph, these turbulent speeds still feel like only 10mph to these majestic creatures; they can control how fast they travel on their own. They prefer to move slowly in order to collect enough energy sources, which is done through the pores on their undersides. Once they've absorbed enough nutrients from their travels, they set out to other distant parts of the planet, fading away into the overcast. More of them will spawn from time to time, but they shouldn't get in your way.

Vukian: Cloud-like aliens that give off strange energy emissions when feeling threatened. They can't see, but they can easily sense when an outsider arrives. They flash a dingy yellow color from their bodies during this state in attempt to ward off potential harm.



 They also do this when feeling excited or reproducing (budding; they are genderless, obviously.)

Kez: Non-sentient creatures resembling two diodes connected. Their natural purpose is to absorb the highly noxious airborne substances that are involuntarily released from the Fluttering Fungi and convert it back into a food source for the Vukian, which is an ongoing process. However, I've taken the liberty of placing them near the Uranian chasm to act as a safeguard; you will need to drive toward them to avoid blindly falling into the chasm.

Golgik: A new Uranian species (albeit only one here as of now) of great size. It has made its home on the largest of the ice platforms. It is unknown to why this creature appeared here. It is the only alien here with sight; a large eye that allows it to easily observe you from afar sits just out of your peripheral, even in such visual obstruction that is the Uranian atmosphere.

Shulu: Octupi-like giants that prefer to conceal themselves in the foggy haze. They eat the donut-shaped pickups, so there aren't any near them.

*The raceline is
 a bit slippery at sloped sections, but your only danger is near the Uranian chasm. You don't have Uranian vision to see through the haze because you're a human, and humans don't have supervision. You'll have to deal with it because you're not supposed to be on Uranus to begin with.


[Important Notes]:

*Uranus was originally supposed to be released in Neptune's place, but hindering circumstances were persistent at the time, ultimately leading to its delay.

*The Uranian world allegedly reeks of pneumonia and putrid rotten eggs.

*There are no Camera Nodes due to the impenetrable fog.

*The pickups are almost entirely absent. They look like crystallized donuts, but they're not donuts. DO NOT EAT! It would seem that the Shulu primarily feed on them. Don't worry. They don't have any interest in Stars, and plenty of pickups exist away from them.

*Cars with excellent suspension will excel here. NY54, Panga TC, Bertha Ballistics, Pest Control, AMW, and Zipper randomly spaz out for unknown reasons. Persistently and patiently adjusting the AI Nodes continued to prove fruitless; they just cannot perform well here.

*Hold your breath because it certainly stinks here (not that your feeble human lungs could perform any form of inhalation here anyway.)

*If you're going to whine, don't. Don't because you won't be able to. You won't be able to because you can't. You can't because you'll already be destroyed before getting a chance.

*Z's are Z's. They drive the Re-Volt Community Cra-Z-y.

*There will be four pick-up stars, and four practice stars.

 Two of the pick-up stars are first come, first serve.

*The ball bearing has become Umbriel; one of Uranus's moons. Incomprehensible sounds come from the inhabitants of Umbriel.
Also, just like for Pluto, the Firework pickups have become icicles, which home in on the target with more precision.

*The horn is not a horn. All cars' engines may have another Uranian lifeform inside.





*The music loops the way 607 wants it to so that he can finally get some decent sleep tonight again.

[*Hints for Practice Star locations*]

1st star: "Cheering you on at the start of the race."
2nd star: "Can you reach me?"

 
3rd star: "Travel through the Golgik's respiratory passage." (An isolated Vukian is holding it.)
4th star: "[Undeciphered Uranian Text...]" (Locate it on your own.)

[*Known Bugs*]
1. The music may static while looping. Unknown reason. Blame 607 for fervently insisting that every song loops.
2. The ground is consumed by a dense fog upon entering the Golgik's mouth. It was unintentionally, but adds too the eccentricity.

[*Tools Used/Credits*]

Blender

Master Blakez
HabitatB Blender Plugin (for Revolt) by Marv (I'z still don't know where your CP pointz are atz.)

Audacity

Visual Boy Advance (Gameboy Advance Emulator)

MAKEITGOOD Edit Mode & Dev Mode

Neopolitan Ice Cream Sammiches.

Nike, Niea,
Johan, Mico,
Leo, Veronica,
Holden, Jenny


Isaac, Felix, Matthew, Himi

Garet, Jenna, Tyrell, Eoleo

Ivan, Sheba, Karis, Sveta

Mia, Piers, Rief, Amiti

Kraden

Saturos, Menardi, Karst, Agatio

Gamelot

NTREEV

Nintendo

VIS Interactive
Hasbro
Kill3rCombo
Sting Entertainment

[*A few of you were overly confident in your foolish assumptions that Banjo-Kazooie would not be in the Smash Bros. Ultimate DLC, and that all Banjo-Kazooie supporters were idiots for wanting them. Your ridicule didn't age too well, did it? Guh-huh~
Each of you know who you are. Ambr-r-r-ree~
https://www.youtube.com/watch?v=ELRgkRMkj6w