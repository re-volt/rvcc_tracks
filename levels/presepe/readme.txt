

----------------------------------------------------------------Christmas Crib 2011--------------------------------------------------------------------------

----------------------------------------------------------------------by Crescviper----------------------------------------------------------------------------------



General nformation:
------------

Name:          Christmas Crib 2011

Author:         Crescviper

Type:            Extreme

Length:         489 m.

Surface:       The surface installed by me with ASE TOOLS are: The Default Surface and the Gravel Surface (in -morph mode)

-------------
Installation:
-------------

You must unzip the Presepe.zip into your main Re-Volt directory.

------------
Descrizione (italiano):
------------

Pista fatta con gli ASE TOOLS in occasione del natale 2011

La pista � ambientata in un tipico presepe natalizio...con pastori, nativit�, alberi di natale, fontanelle, lampadine colorate... insomma tutto ci� che si possa trovare in un presepe di natale =D
Questa pista � stata fatta in 2/3 settimane xD

------------------------------------------------------------------------------------------------------------------
------------

Description (English):
------------

Track made with ASE TOOLS in occasion of Christmas 2011

The track is set in a traditional Christmas crib...with shepherds, nativity scene, Christmas tree, fountains, colored lights...so everything that you can find in a traditional Christmas crib =D
This track was made in 2/3 week xD

----------------------------------------

Programs used by me for make this track:

----------------------------------------

3dsmax
ASE TOOLS
Makeitgood
RV-SHADE
Zmodeler
Paint of windows XP
Gimp
Artweaver
Re-volt 1.2 UPDATE

---------------------------------------

Special thanks to the italian site "Aliasrevoltmaster" and the english site "RVZT"


Copyright by Crescviper 2011...All right reserved
