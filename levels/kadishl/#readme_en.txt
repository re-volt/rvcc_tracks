﻿╔═══╦═══════╦═══
║   ║SUMMARY║
║   ╚═══════╝
║
║ Name:			Kadish Basic
║ Folder Name:		kadishl
║ Author:		Dyspro50
║ Category:		Lego
║ Lenght:		829 meters
║ Date Completed:	November 25, 2017
║
║ Tools:		MAKEITGOOD
║			Re-Volt Track Editor with v1.1 update by Kenny
║			Blender 2.73 with Jigebren's Re-Volt Plugin (v2015-02-27)
║			rvtmod8
║			rvglue
║			PaintDotNet
║
╚══════════════

╔═══╦═════════════════╦═══
║   ║TRACK DESCRIPTION║
║   ╚═════════════════╝
║
║ "Kadish Basic" is a lego track that re-use assets from its Extreme counterpart, Kadish Sprint.
║
║ Reverse mode will be available later.
║ 
╚═════════════════════════

╔═══╦════════════╦═══
║   ║KNOWN ISSUES║
║   ╚════════════╝
║
║ None so far...
║
╚════════════════════

╔═══╦═════╦═══
║   ║NOTES║
║   ╚═════╝
║	
║
║	- Track music is from the kadishs folder and only works if you already downloaded Kadish Sprint.
║
║	- This track is only compatible with RVGL. Make sure to always have the latest version of RVGL.
║	However, it may be compatible Re-Volt 1.2 by converting textures files to 24-bit RGB. 
║
║	- Every meshes (M, PRM, W) except those below are done by me.
║	  You can reuse any of them if you want, but only if you credits me.
║
║		- Tree walls maded using CANYON1, *2, *I and *O.PRM, from JimK Offroad Kit, edited by Dyspro50.
║
║	- Every textures and sounds (Except some default Track Editor textures) are from URU.
║
║		- You are allowed to reuse any files only if you credit their respective author(s).
║
║	- If you have any questions, please contact me on Re-Volt Live, or by e-mail at dyspro50@gmail.com
║
╚═════════════

╔═══╦═════════════════╦═══
║   ║SPECIAL THANKS TO║
║   ╚═════════════════╝
║
║	- Cyan Worlds for the Myst video game series that inspired this track
║
║	- You for downloading this track !
║
╚═════════════════════════


 Dyspro


********************************************************************************************************
