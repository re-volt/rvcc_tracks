
*** THIS IS THE FRENCH VERSION OF THE README. FOR ENGLISH, PLEASE SEE #readme_en.txt ***

+--------------
�   �R�SUMɦ
�   +------+
�
� Nom:			Kadish Basic
� Nom du dossier:	kadishl
� Auteur:		Dyspro50
� Cat�gorie:		Lego
� Longueur:		829 m�tres
� Date de compl�tion:	25 novembre 2017
�
� Outils:		MAKEITGOOD
�			Re-Volt Track Editor avec la mise � jour update v1.1 par Kenny
�			Blender 2.73 avec le plug-in de Jigebren pour Re-Volt (v2015-02-27)
�			rvtmod8
�			rvglue
�			PaintDotNet
�
+--------------

+-------------------
�   �DESCRIPTION�
�   +-----------+
�
� �Kadish Basic� est un circuit lego r�utilisant des instances du circuit Kadish Sprint.
�
� Le mode � l'envers (Reversed) sera disponible plus tard.
�
+-------------------

+------------------------
�   �PROBL�MES CONNUS�
�   +----------------+
�
� Aucun jusqu'� date...
�
+------------------------

+-------------
�   �NOTES�
�   +-----+
�	
�
�	- La musique est lue depuis le dossier kadishs et ne fonctionne que si vous poss�dez Kadish Sprint.
�
�	- Ce circuit n'est compatible qu'avec RVGL. Toujours s'assurer d'avoir la derni�re version de RVGL.
�
�	- Toutes les mailles (M, PRM, W) except� ceux sp�cifi�es ci-dessous sont con�ues par moi.
�	  Vous avez l'autorisation de les r�utiliser comme vous le voulez, mais seulement si vous mentionnez
�	  son auteur original.
�
�		- Mur d'arbres bas�s sur les fichiers CANYON1, *2, *I and *O.PRM, du JimK Offroad Kit, �dit� par Dyspro50.
�
�	- Toutes les textures et les sons (Except� quelques textures par d�faut de l'�diteur de circuit) proviennent d'URU.
�		
�		- Vous pouvez les r�utiliser seulement si vous cr�ditex son auteur original.
�
�	- Pour toutes questions, S.V.P. contactez-moi sur Re-Volt Live, ou par courriel � l'adresse dyspro50@gmail.com.
�
+-------------

+--------------------------
�   �REMERCIMENTS �/AUX�
�   +------------------+
�
�	- Cyan Worlds pour la s�rie de jeux vid�o Myst, qui a inspir� ce circuit
�
�	- Vous pour avoir t�l�charg� le circuit !
�
+--------------------------


 Dyspro


********************************************************************************************************
