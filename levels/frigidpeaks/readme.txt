Track name: Frigid Peaks
Install in folder: Re-volt\levels\FrigidPeaks
Author: Mace (aka: Mace121)
Email Address: Macethe0mni@gmail.com


Description:

The definition of Frigid (fri-jid): It's cold AF. The mountains of Frigid Peaks from Kemco's seminal "set your car ablaze at 200mph off the track" simulator, Top Gear Overdrive is track #1 in the game. I used to play this as a wee kid, and it's reportedly one of the very first games I've ever played, at the age of 2. 

This track is dubbed Figid Peaks, but in the manual it's known as Mountain 22... Yeah. The initial result before I started reconstructing looks rough, so I decided to throw in my "7 Secret Herbs" to make it worthwhile and make it less dull. Even though it's a "coversion," there are elements I have tampered with, like the taller barriers, some smoother roads, and lightly modified textures, but not all that damaging. I still ripped the models from the game, remapped the textures, and... Oh yeah, used said textures.

Sadly, Vivi was meant to be there, but cut due to the unfriendliness of transluscent meshes. Blame the renderer.

______________________________________________________________________________________

Some Info:

-Track length: 370m
-Actual track length (w/o shortcuts): 470m

Music:
-Grindstone: Hollow Eyes


The Grindstone Mixtape (in-development):
-Hollow Eyes
-Threshold
-Mir


Misc.

The Track isn't exactly the 1:1 thing I'd hoped, but I did my best to make a decent conversion. 

______________________________________________________________________________________

* Copyright / Permissions *

Track models & Textures by Kemco & Snowblind Studios
Music by Grindstone
Custom sounds from Freesound

Yeah, sure. Why not, unless you credited the author(s).
