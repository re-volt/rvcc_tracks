{
;==============================================================================
;  RVGL LEVEL PROPERTIES
;
;  This file allows modification of these level properties:
;  - Surface materials
;  - Dust (graphical effects for materials)
;  - Corrugation (material bump effects)
;  - Particles (sparks of material effects and other objects)
;  - Trails of particles (like those of fireworks)
;  - Gravity (modify the behavior of the global gravity field)
;  - Pickups (modify pickup spawning and visual parameters)
;==============================================================================


;==============================================================================
;  MATERIALS
;==============================================================================
;  Default material IDs:
;    NONE:       -1  |  BUMPMETAL:         13
;    DEFAULT:     0  |  PEBBLES:           14
;    MARBLE:      1  |  GRAVEL:            15
;    STONE:       2  |  CONVEYOR1:         16
;    WOOD:        3  |  CONVEYOR2:         17
;    SAND:        4  |  DIRT1:             18
;    PLASTIC:     5  |  DIRT2:             19
;    CARPETTILE:  6  |  DIRT3:             20
;    CARPETSHAG:  7  |  ICE2:              21
;    BOUNDARY:    8  |  ICE3:              22
;    GLASS:       9  |  WOOD2:             23
;    ICE1:       10  |  CONVEYOR_MARKET1:  24
;    METAL:      11  |  CONVEYOR_MARKET2:  25
;    GRASS:      12  |  PAVING:            26
;==============================================================================
;==============================================================================

MATERIAL {
  ID              3                             ; Material to replace [0 - 26]
  Name            "Ice shit"                        ; Display name

  Skid            true                         ; Skidmarks appear on material
  Spark           false                         ; Material emits particles
  OutOfBounds     false                         ; Not implemented
  Corrugated      false                         ; Material is bumpy
  Moves           false                         ; Moves like museum conveyors
  Dusty           false                         ; Material emits dust

  Roughness       0.300000                      ; Roughness of the material
  Grip            0.300000                      ; Grip of the material
  Hardness        0.050000                      ; Hardness of the material

  SkidSound       6                             ; Sound when skidding
  ScrapeSound     5                             ; Car body scrape [5:Normal]

  SkidColor       16 32 96                      ; Color of the skidmarks
  CorrugationType 0                             ; Type of bumpiness [0 - 7]
  DustType        0                             ; Type of dust
  Velocity        0.000000 0.000000 0.000000    ; Move cars
}


MATERIAL {
  ID              9                             ; Material to replace [0 - 26]
  Name            "Jon Snow"                       ; Display name

  Skid            true                          ; Skidmarks appear on material
  Spark           true                          ; Material emits particles
  OutOfBounds     false                         ; Not implemented
  Corrugated      true                         ; Material is bumpy
  Moves           false                         ; Moves like museum conveyors
  Dusty           true                         ; Material emits dust

  Roughness       0.500000                      ; Roughness of the material
  Grip            0.700000                      ; Grip of the material
  Hardness        0.150000                      ; Hardness of the material

  SkidSound       7                             ; Sound when skidding
  ScrapeSound     5                             ; Car body scrape [5:Normal]

  SkidColor       24 64 128                      ; Color of the skidmarks
  CorrugationType 7                             ; Type of bumpiness [0 - 7]
  DustType        5                             ; Type of dust
  Velocity        0.000000 0.000000 0.000000    ; Move cars
}

MATERIAL {
  ID              10                            ; Material to replace [0 - 26]
  Name            "ICE1"                        ; Display name

  Skid            true                          ; Skidmarks appear on material
  Spark           true                          ; Material emits particles
  OutOfBounds     false                         ; Not implemented
  Corrugated      false                         ; Material is bumpy
  Moves           false                         ; Moves like museum conveyors
  Dusty           false                         ; Material emits dust

  Roughness       0.400000                      ; Roughness of the material
  Grip            0.400000                      ; Grip of the material
  Hardness        0.200000                      ; Hardness of the material

  SkidSound       7                             ; Sound when skidding
  ScrapeSound     5                             ; Car body scrape [5:Normal]

  SkidColor       24 64 128                      ; Color of the skidmarks
  CorrugationType 0                             ; Type of bumpiness [0 - 7]
  DustType        0                             ; Type of dust
  Velocity        0.000000 0.000000 0.000000    ; Move cars
}


MATERIAL {
  ID              20                            ; Material to replace [0 - 26]
  Name            "Dirtsnow"                       ; Display name

  Skid            true                         ; Skidmarks appear on material
  Spark           true                         ; Material emits particles
  OutOfBounds     false                         ; Not implemented
  Corrugated      true                          ; Material is bumpy
  Moves           false                         ; Moves like museum conveyors
  Dusty           true                          ; Material emits dust

  Roughness       1.000000                      ; Roughness of the material
  Grip            0.700000                      ; Grip of the material
  Hardness        0.000000                      ; Hardness of the material

  SkidSound       7                             ; Sound when skidding
  ScrapeSound     5                             ; Car body scrape [5:Normal]

  SkidColor       96 128 160                   ; Color of the skidmarks
  CorrugationType 4                             ; Type of bumpiness [0 - 7]
  DustType        5                             ; Type of dust
  Velocity        0.000000 0.000000 0.000000    ; Move cars
}

MATERIAL {
  ID              23                            ; Material to replace [0 - 26]
  Name            "Icy stuff"                       ; Display name

  Skid            true                         ; Skidmarks appear on material
  Spark           false                         ; Material emits particles
  OutOfBounds     false                         ; Not implemented
  Corrugated      false                         ; Material is bumpy
  Moves           false                         ; Moves like museum conveyors
  Dusty           false                         ; Material emits dust

  Roughness       0.8500000                      ; Roughness of the material
  Grip            0.700000                      ; Grip of the material
  Hardness        0.400000                      ; Hardness of the material

  SkidSound       6                             ; Sound when skidding
  ScrapeSound     5                             ; Car body scrape [5:Normal]

  SkidColor       48 48 48                      ; Color of the skidmarks
  CorrugationType 0                             ; Type of bumpiness [0 - 7]
  DustType        0                             ; Type of dust
  Velocity        0.000000 0.000000 0.000000    ; Move cars
}


;==============================================================================
;  CORRUGATION TYPES
;==============================================================================
;  Default corrugation IDs:
;    NONE:     0  |  CONVEYOR:  4
;    PEBBLES:  1  |  DIRT1:     5
;    GRAVEL:   2  |  DIRT2:     6
;    STEEL:    3  |  DIRT3:     7
;==============================================================================
;==============================================================================

CORRUGATION {
  ID              4                             ; Corrugation to replace [0-7]
  Name            "DIRTSnow"                       ; Display name

  Amplitude       1.7500000                      ; Amplitude of bumps (strength)
  Wavelength      60.000000 50.000000           ; Frequency of bumps
}

CORRUGATION {
  ID              7                             ; Corrugation to replace [0-7]
  Name            "DIRT3"                       ; Display name

  Amplitude       2.300000                      ; Amplitude of bumps (strength)
  Wavelength      120.000000 120.000000           ; Frequency of bumps
}


;==============================================================================
;  DUST TYPES
;==============================================================================
;  Default dust IDs:
;    NONE:    0  |  GRASS:  3
;    GRAVEL:  1  |  DIRT:   4
;    SAND:    2  |  ROAD:   5
;==============================================================================
;==============================================================================

}

DUST {
  ID              5                             ; Dust to replace [0 - 5]
  Name            "Snow"                        ; Display name

  SparkType       5                             ; Particle to emit [0 - 30]
  ParticleChance  1.000000                      ; Probability of a particle
  ParticleRandom  1.800000                      ; Probability variance

  SmokeType       30                            ; Smoke particle to emit [0-30]
  SmokeChance     0.400000                      ; Probability of a smoke part.
  SmokeRandom     0.600000                      ; Probability variance

}


;==============================================================================
;  SPARKS
;==============================================================================
;  Default spark IDs:
;    NONE:     -1  |  SMOKE1:       10  |  SPRINKLER:      21
;    SPARK:     0  |  SMOKE2:       11  |  SPRINKLER_BIG:  22
;    SPARK2:    1  |  SMOKE3:       12  |  DOLPHIN:        23
;    SNOW:      2  |  BLUE:         13  |  DOLPHIN_BIG:    24
;    POPCORN:   3  |  BIGBLUE:      14  |  SPARK3:         25
;    GRAVEL:    4  |  SMALLORANGE:  15  |  ROADDUST:       26
;    SAND:      5  |  SMALLRED:     16  |  GRASSDUST:      27
;    GRASS:     6  |  EXPLOSION1:   17  |  SOILDUST:       28
;    ELECTRIC:  7  |  EXPLOSION2:   18  |  GRAVELDUST:     29
;    WATER:     8  |  STAR:         19  |  SANDDUST:       30
;    DIRT:      9  |  PROBE_SMOKE:  20  |
;==============================================================================
;==============================================================================


SPARK {
  ID              5                             ; Particle to replace [0 - 30]
  Name            "Snow"                       ; Display name

  CollideWorld    true                          ; Collision with the world
  CollideObject   true                          ; Collision with objects
  CollideCam      true                          ; Collision with camera
  HasTrail        true                         ; Particle has a trail
  FieldAffect     true                          ; Is affected by force fields
  Spins           true                         ; Particle spins
  Grows           false                         ; Particle grows
  Additive        true                          ; Draw particle additively
  Horizontal      false                         ; Draw particle flat

  Size            15.000000 12.000000           ; Size of the particle
  UV              0.750000 0.062500             ; Top left UV coordinates
  UVSize          0.062500 0.062500             ; Width and height of UV
  TexturePage     47                            ; Texture page
  Color           192 220 255                     ; Color of the particle

  Mass            0.030000                      ; Mass of the particle
  Resistance      0.001000                      ; Air resistance
  Friction        0.800000                      ; Sliding friction
  Restitution     0.200000                      ; Bounciness

  LifeTime        2.000000                      ; Maximum life time
  LifeTimeVar     1.000000                      ; Life time variance

  SpinRate        0.000000                      ; Avg. spin rate (radians/sec)
  SpinRateVar     0.000000                      ; Variation of the spin rate

  SizeVar         0.000000                      ; Size variation
  GrowRate        0.000000                      ; How quickly it grows
  GrowRateVar     0.000000                      ; Grow variation

  TrailType       0                            ; ID of the trail to use
}