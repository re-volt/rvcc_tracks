          			         ╒═══════════════════╗
╓────────────────────────────────────────┤Track information  ║
║         			         └──────────────────┬╜
║ Track name          : A Snowy Night Out West              │
║ Track Type  	      : Extreme                             │
║ Length 	      : 344m                                │
╟───────────────────────────────────────────────────────────┤
║ Description                                               │
║ ¨¨¨¨¨¨¨¨¨¨¨         A winter-themed track for the         │
║                     Re-Volt Live Christmas Championship   │
║                     2011, and also a part for a mod       │
║                     very soon. Hard AI, two stars in      │
║                     place. This track is availalbe on the │
║                     following websites:                   │
║                     http://z3.invisionfree.com/Revolt_Live│                           
║                     http://rvzt.net                       │
║                                                           │
║                        Thank you                          │
║                               for downloading!            │
║                                                           │
╚───────────────────────────────────────────────────────────╛

          			         ╒═══════════════════╗
╓────────────────────────────────────────┤Author information ║
║         			         └──────────────────┬╜
║ E-mail Adresses     : miromiro - mironutz_boy@yahoo.com   │
║   of the authors!   : Dave_o_rama - davedontalk@gmail.com |
║                     : R6TE -  thomaswestzaan@hotmail.com  │       
║ 	                                                    │
╚───────────────────────────────────────────────────────────╛


          			         ╒═══════════════════╗
╓────────────────────────────────────────┤  Construction     ║
║         			         └──────────────────┬╜
║  Editor(s) used      : Rv-Glue, RV-Remap, RV-sizer,       │
║                        3Ds Max, MAKEITGOOD Edit Modes,   │
║ 	                 MKMirror & GIMP                   │
╚───────────────────────────────────────────────────────────╛
 

          			         ╒═══════════════════╗
╓────────────────────────────────────────┤Additional Credits ║
║         			         └──────────────────┬╜
║ Thanks to Acclain for creating Re-Volt, Jigebren for      │
║ WolfR4, Huki & Jigebren for developing Re-Volt 1.2, The   │
║ Re-Volt Live forums, The Internet for most of the textures│
║ and what not, and you for downloading and hopefully will  │
║ like this track as you cared to open this damn ReadMe!    │	                            │
╚───────────────────────────────────────────────────────────╛


          			         ╒═══════════════════╗
╓────────────────────────────────────────┤   Copyrights      ║
║         			         └──────────────────┬╜
║ You may do whatever you want with this Track. Provided you│
║ include this file, with NO modifications.                 │
║ Copyrighted © Saturday November 5th, 2011 01:11 AM        │
╚───────────────────────────────────────────────────────────╛