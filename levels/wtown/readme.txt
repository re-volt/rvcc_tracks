Winter Town by Pranav

This is the first track i have released. This track is a conversion of custom track made for Midtown Madness 2. You can find the original track at the mm2x website.
Since this is my first track conversion, there can be a few mistakes.

The track uses some custom features, so i recommend you to download the latest patch for 1.2.

I would like to thank all those who helped me in importing this track.
-My special thanks to Jig, without whom it would not have been possible to make this track.
-Thanks to miro for such a wonderful tutorial on AI nodes.
-Thanks to Dave for helping me out when i am getting stuck at any point.
-Thanks to all the RVL members for always being there to appreciate my work. :)

The music is from Super Meat Boy!

I hope you like it! 


